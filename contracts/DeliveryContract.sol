pragma solidity ^0.4.0;

contract DeliveryContract {

    string public fromLocation;
    string public toLocation;
    string public size;
    string public weight;
    string public volume;

    uint public dueTimestampDelivery;
    uint public closeTimestampAuction;
    uint public pickupTimestamp;
    uint public deliveryTimestamp;

    address public owner;
    address public receiver;
    address public deliverer;
    
    struct Bid {
        uint value;
        address bidder;
        uint timestamp;
    }

    Bid[] public bids;

    modifier onlyRequester() {
        require(msg.sender == owner);
        _;
    }

    modifier onlyReceiver() {
        require(msg.sender == receiver);
        _;
    }

    modifier notOwner() {
        require(msg.sender != owner);
        _;
    }

    event AuctionCreated(string _fromLocation, string _toLocation, string _size, uint _dueTimestampDelivery, address _receiver);
    event NewBid(uint _value, address _bidder, uint _timestamp);
    event AuctionClosed(uint _highestbid, address _deliverer);
    event PickUp(uint _timestamp);
    event Delivery(uint _timestamp);

    function DeliveryContract(string _fromLocation, string _toLocation, string _size, uint _dueTimestampDelivery, address _receiver) public {
        owner = msg.sender;
        fromLocation = _fromLocation;
        toLocation = _toLocation;
        size = _size;
        dueTimestampDelivery = _dueTimestampDelivery;
        receiver = _receiver;
    }

    function bid(uint _value, uint _timestamp) public {
        bids.push(Bid(_value, msg.sender, _timestamp));
        NewBid(_value, msg.sender, _timestamp);
    }

    function closeAuction(uint _timestamp) public {
        if (bids.length > 0) {
            Bid memory bestBid = bids[0];
            for (uint index = 1; index < bids.length; index++) {
                if (bids[index].value > bestBid.value) {
                    bestBid = bids[index];
                }
            }
            closeTimestampAuction = _timestamp;
            deliverer = bestBid.bidder;
            AuctionClosed(bestBid.value, bestBid.bidder);
        }
    }

    function pickup(uint _timestamp) {
        pickupTimestamp = _timestamp;
        PickUp(_timestamp);
    }

    function delivery(uint _timestamp) {
        deliveryTimestamp = _timestamp;
        Delivery(_timestamp);
    }

}